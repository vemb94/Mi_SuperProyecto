class AffiliatesController < ApplicationController
  # before_action :find_affiliate, only: [:create, :new, :update, :show, :destroy, :edit]
  skip_before_action :verify_authenticity_token
    REALM = "adalpada"
  ADMIN={"admin"=>"admin"}
  before_action :find_affiliate, only: [:update, :show, :destroy,:edit] 
  before_action only: [:create, :destroy, :new, :update, :edit]do
    authenticate_or_request_with_http_digest(REALM) do |admin|
      ADMIN["admin"]
    end
  end 
  def index
    @affiliate = Affiliate.all
  end

  def show 
  end

  def home  
    @affiliate = Affiliate.query
    render layout: "hpage"
    # render json: @affiliate
  end

  def new
    @affiliate = Affiliate.new
  end

  def create
    @affiliate = Affiliate.new(affiliate_params)
    if @affiliate.save
      redirect_to affiliates_path
    else
      render new, notice: 'no se pudo enviar el fomulario'
    end
  end
  
  def update
  # @product =  Product.find(params[:id])
   #@product.quantity = @product.quantity -1
   #@product.save

   #render json: @product
      
  end

  def destroy
    @affiliate.destroy
    respond_to do |format|
      format.html { redirect_to affiliates_path, notice: 'Affiliate fue eliminado con exito' }
      # format.json { head :no_content }
    end
  end
private

  def affiliate_params
    params.require(:affiliate).permit(:name,:category, :url)
  end
  def find_affiliate
    @affiliate = Affiliate.find_by(id: params[:id])
  end

end
