class ProductsController < ApplicationController
  # before_action :find_product, only: [:create, :new, :update, :show, :destroy,:edit]
  skip_before_action :verify_authenticity_token
   REALM = "adalpada"
  ADMIN = {"admin"=>"admin"}
  before_action :find_product, only: [:update, :show, :destroy,:edit, :counter] 
  before_action only: [:create, :destroy, :new, :update, :edit]do
    authenticate_or_request_with_http_digest(REALM) do |admin|
      ADMIN["admin"]
    end
  end 

    def index
    @product = Product.all

    respond_to do |format|
      format.html
      format.json { render json: @product }
    end
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      puts "hola"
      redirect_to products_path
    else
      render new, notice: 'no se pudo enviar el fomulario'
    end
  end

  def show
    @product
  end

    def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_path, notice: 'El producto fue elminado con exito.' }
      # format.json { head :no_content }
    end
  end

  def edit
    @product
  end

    def counter
    @product = Product.find_by(id: params[:id])
    a = @product.quantity-1
    @product.quantity = a
    @product.save
    render json: @product.quantity

    #ahhhhhhhhhhhhhhh
  end

  def update
    if @product.update(product_params)
      redirect_to product_path
    else
      render 'edit'
    end
  end

  private

  def product_params
    params.require(:product).permit(:store, :name, :description, :price, :quantity, :category, :image)
    # falta inmagen
  end

  def find_product
    @product = Product.find_by(id: params[:id])
  end
end

