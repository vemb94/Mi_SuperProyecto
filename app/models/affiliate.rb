# == Schema Information
#
# Table name: affiliates
#
#  id         :integer          not null, primary key
#  name       :string
#  category   :string
#  url        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Affiliate < ApplicationRecord
  def self.query
    @Tiendas = Affiliate.all
    @response = []
    @Tiendas.each do |tienda|
      @url = RestClient.get(tienda.url)
      @response << JSON.parse(@url.body)
      @mamalo = []
    end
    @response.each do |array|
      array.each do |json|
        @mamalo << json
      end
    end
    @mamalo
end
end
