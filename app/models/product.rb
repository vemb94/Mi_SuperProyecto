# == Schema Information
#
# Table name: products
#
#  id         :integer          not null, primary key
#  store      :string
#  name       :string
#  descripton :string
#  price      :integer
#  quantity   :integer
#  category   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Product < ApplicationRecord

  has_attached_file :image , styles: {large: "50x50>" , medium:  "180x150" , thumb:  "25x25>" }, default_url:  "/images/missing.png" 
  validates_attachment_content_type :image , content_type:  /\Aimage\/.*\Z/
end
