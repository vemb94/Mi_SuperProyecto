Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :products
  resources :affiliates
  root to: "affiliates#home"
  post 'products/:id', to: 'products#counter'
end
