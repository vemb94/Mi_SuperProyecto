class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :store
      t.string :name
      t.string :description
      t.integer :price
      t.integer :quantity
      t.string :category

      t.timestamps
    end
  end
end